PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
VERSION := ${VERSION}
APTLY_USER := ${APTLY_USER}
APTLY_PASSWORD := ${APTLY_PASSWORD}

.PHONY: all build clean test coverage coverhtml lint upx fpm_install fpm fpm_upload_dev fpm_upload_tag

all: build

lint: ## Lint the files
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install
	gometalinter --enable=goimports --enable=lll --enable=misspell --enable=nakedret --enable=unparam --enable=safesql ${PKG_LIST}

test: ## Run unittests
	go test -short ${PKG_LIST}

race: ## Run data race detector
	go test -race -short ${PKG_LIST}

msan: ## Run memory sanitizer
	CC=clang-5.0 go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

build: ## Build the binary file
	go build -ldflags "-w -s -extldflags '-static' -X main.versionString=$(VERSION)"

upx: ## Compress binary with UPX
	apt-get update && apt-get install xz-utils
	curl -LO https://github.com/upx/upx/releases/download/v3.94/upx-3.94-amd64_linux.tar.xz
	tar -xf upx-3.94-amd64_linux.tar.xz
	mv upx-3.94-amd64_linux/upx /usr/local/bin/
	upx --brute go-init

fpm_install:
	apt-get update -y && apt-get install ruby ruby-dev rubygems build-essential -y
	gem install --no-ri --no-rdoc fpm

fpm:
	fpm -s dir -t deb -n go-init -m "contact@pablo-ruth.fr" --url "https://gitlab.com/pablo-ruth/go-init" --description "A minimalist init system for containers" --category "admin" -v $(VERSION) --prefix /usr/bin go-init

fpm_upload_dev:
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST -F file=@go-init_$(VERSION)_amd64.deb https://deb.pablo-ruth.fr/api/files/go-init_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST https://deb.pablo-ruth.fr/api/repos/go-init-dev/file/go-init_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X PUT -H 'Content-Type: application/json' --data '{"Signing": {"Skip": true}}' https://deb.pablo-ruth.fr/api/publish/go-init-dev/xenial
	@curl -f -o /dev/null --silent --head https://deb.pablo-ruth.fr/go-init-dev/pool/main/g/go-init/go-init_$(VERSION)_amd64.deb
	
fpm_upload_tag:
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST -F file=@go-init_$(VERSION)_amd64.deb https://deb.pablo-ruth.fr/api/files/go-init_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST https://deb.pablo-ruth.fr/api/repos/go-init/file/go-init_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X PUT -H 'Content-Type: application/json' --data '{"Signing": {"Skip": true}}' https://deb.pablo-ruth.fr/api/publish/go-init/xenial
	@curl -f -o /dev/null --silent --head https://deb.pablo-ruth.fr/go-init/pool/main/g/go-init/go-init_$(VERSION)_amd64.deb

help: ## Display this help screen
	grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
